// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:flutter_questionnaire/main.dart';

void main() {
  testWidgets('Test activity', (WidgetTester tester) async {
    // Load the widget to the test
    await tester.pumpWidget(App());

    await tester.tap(find.text('Asset Management'));
    await tester.pump();

    await tester.tap(find.text('More Than 10,000'));
    await tester.pump();

    await tester.tap(find.text('Europe'));
    await tester.pump();

    await tester.tap(find.text('6-9 months'));
    await tester.pump();

    expect(find.text('What is the nature of your business?'), findsOneWidget);
    expect(find.text('Asset Management'), findsOneWidget);

    expect(find.text('What is the expected size of user base?'), findsOneWidget);
    expect(find.text('More Than 10,000'), findsOneWidget);

    expect(find.text('In which region would the majority of the user base be?'), findsOneWidget);
    expect(find.text('Europe'), findsOneWidget);

    expect(find.text('What is the expected project duration?'), findsOneWidget);
    expect(find.text('6-9 months'), findsOneWidget);
    
    });
}
